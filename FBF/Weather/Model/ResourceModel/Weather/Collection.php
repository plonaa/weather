<?php

namespace FBF\Weather\Model\ResourceModel\Weather;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \FBF\Weather\Model\Weather;
use \FBF\Weather\Model\ResourceModel\Weather as WeatherResource;

class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'fbf_weather_weather';
    protected $_eventObject = 'collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Weather::class, WeatherResource::class);
    }
}
