<?php


namespace FBF\Weather\Cron;

use FBF\Weather\Model\ImportWeather as ImportWeatherModel;
use Psr\Log\LoggerInterface;

/**
 * Class ImportWeather
 * @package FBF\Weather\Cron
 */
class ImportWeather
{
    /**
     * @var ImportWeatherModel
     */
    protected $weatherModel;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * ImportWeather constructor.
     * @param ImportWeatherModel $weatherModel
     * @param LoggerInterface $logger
     */
    public function __construct(
        ImportWeatherModel $weatherModel,
        LoggerInterface $logger
    )
    {
        $this->weatherModel = $weatherModel;
        $this->logger = $logger;
    }

    /**
     * Import Current Weather with API
     */
    public function execute()
    {
        try {
            $this->weatherModel->importWeather();
        } catch (\Exception $e) {
            $this->logger->critical('fbf_weather_cron faild with message: ' . $e->getMessage());
        }
    }
}
